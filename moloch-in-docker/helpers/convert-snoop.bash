#!/bin/bash     

root="/data/pcaps/spool"                                                                                                              
dir="${root}/$(dirname $2)"                                                                                                           
name=$(basename $2)                                                                                                                   
dname=$(dirname $2)                                                                                                                   
src="$1/${dname}/${name}"                                                                                                             
dst="${dir}/${name}.pcap"                                                                                                             

if [ ! -d "${dir}" ] ; then                                                                                                           
 mkdir -p "${dir}"                                                                                                                    
 /root/scripts/new-dir-from-convert.bash "$2" &                                                                                       
fi                                                                                                                                    

if [ -f "${dst}" ]; then                                                                                                              
  echo "$(date) $1 $2 $src $dst" >> /var/log/pcaps-convert-skip.log                                                                   
else                                                                                                                                  
  echo "$(date) $1 $2 $src $dst" >> /var/log/pcaps-convert.log
  tshark -r "${src}" -F pcap -w "${dst}" || echo "$2" >> /var/log/pcaps-convert.notpcap                                               
fi                                                                                                                                    

rm "${src}"                       